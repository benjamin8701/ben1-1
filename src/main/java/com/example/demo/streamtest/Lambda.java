package com.example.demo.streamtest;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;

/**
 * @program: ben1-1
 * @description: lambda
 * @author: wb
 * @create: 2021-07-27 08:45
 **/
public class Lambda {

    public static void main(String[] args) {
        FilenameFilter c = (File file, String name) -> name.endsWith(".md");
        File file = new File("d:/");
        for (String s : file.list()) {
            c.accept(file, s);
        }
    }

    public void numbers(){
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        numbers.parallelStream()
                .forEachOrdered(System.out::println);
    }

}
