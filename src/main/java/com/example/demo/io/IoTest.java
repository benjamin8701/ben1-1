package com.example.demo.io;




import org.apache.commons.lang3.StringUtils;

import java.io.*;

/**
 * @program: ben1-1
 * @description: io
 * @author: wb
 * @create: 2021-08-04 08:58
 **/
public class IoTest {

    public static void main(String[] args) {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("d://abc.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            System.out.println(inputStreamReader);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String s = bufferedReader.readLine();
            if (StringUtils.isNotBlank(s)) {
                System.out.println(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
