package com.example.demo.strategy;

import java.math.BigDecimal;

/**
 * @program: ben1-1
 * @description: 用户2
 * @author: wb
 * @create: 2021-08-03 15:19
 **/
public class MemberSecond implements Member {
    @Override
    public BigDecimal salePrice(BigDecimal price) {
        return price.multiply(BigDecimal.valueOf(0.6));
    }
}
