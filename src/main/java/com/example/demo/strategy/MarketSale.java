package com.example.demo.strategy;

import java.math.BigDecimal;

/**
 * @program: ben1-1
 * @description: 销售
 * @author: wb
 * @create: 2021-08-03 15:28
 **/
public class MarketSale {

    public static void main(String[] args) {
        Member member = new MemberFirst();

        Price price = new Price(member);

        BigDecimal quote = price.quote(BigDecimal.valueOf(100));

        System.out.println(quote);
    }
}
