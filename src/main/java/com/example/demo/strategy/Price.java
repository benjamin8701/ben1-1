package com.example.demo.strategy;

import java.math.BigDecimal;

/**
 * @program: ben1-1
 * @description: 价格
 * @author: wb
 * @create: 2021-08-03 15:23
 **/
public class Price {

    private Member member;

    public Price(Member member){
        this.member = member;
    }

    public BigDecimal quote(BigDecimal salePrice){
        return this.member.salePrice(salePrice);
    }
}
