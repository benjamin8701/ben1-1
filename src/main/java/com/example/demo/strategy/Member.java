package com.example.demo.strategy;

import java.math.BigDecimal;

/**
 * @author: wb
 * @date:  2021/8/3 15:18
*/
public interface Member {

    public BigDecimal salePrice(BigDecimal price);
}
