package com.example.demo.functionInterface;

import java.util.function.Consumer;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-11 11:36
 **/
public class ConsumerTest {

    public static void main(String[] args) {
        User user = new User();
        user.setAge(10);
        Consumer<User> u = e -> e.setName("aaa");
        System.out.println(user);
        u.accept(user);
        System.out.println(user);
    }

}
