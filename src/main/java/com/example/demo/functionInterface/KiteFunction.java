package com.example.demo.functionInterface;

/**
 * @author benjamin
 */
@FunctionalInterface
public interface KiteFunction<T,R,S> {

    /**
     * run
     *
     * @param r r
     * @param s s
     * @return R r
     */
    T fun(R r,S s);
}
