package com.example.demo.functionInterface;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @program: ben1-1
 * @description: 自定义函数式接口
 * @author: wb
 * @create: 2021-07-26 15:59
 **/
public class UsedFunInt {

    public static void main(String[] args) {
        KiteFunction<String, LocalDateTime, String> functionDateFormat = (LocalDateTime l, String d) -> {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(d);
            return l.format(dateTimeFormatter);
        };
        String fun = functionDateFormat.fun(LocalDateTime.now(), "yyyy-MM-dd HH:mm:ss");
    }
}
