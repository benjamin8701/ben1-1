package com.example.demo.functionInterface;

import lombok.Data;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-11 11:33
 **/
@Data
public class User {
    private String name;
    private int age;
    private int sex;
}
