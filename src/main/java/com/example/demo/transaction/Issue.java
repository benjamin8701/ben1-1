package com.example.demo.transaction;

import cn.hutool.extra.spring.SpringUtil;
import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-09 15:57
 **/
@Data
public class Issue {

    private String id;

    private String issueName;

    private String status;

    private StatusModify statusModify;
}
