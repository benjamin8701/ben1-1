package com.example.demo.transaction;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-09 16:04
 **/
public interface IssueStrategy {

    boolean isMatched(StatusModify statusModify);

    void handle(Issue issue);
}
