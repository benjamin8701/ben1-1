package com.example.demo.transaction;

import cn.hutool.json.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.List;
import java.util.Objects;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-09 16:04
 **/
@Component
@Slf4j
public class CreateIssueSuccessStrategy implements IssueStrategy {


    @Override
    public boolean isMatched(StatusModify statusModify) {
        if (Objects.isNull(statusModify)) {
            return false;
        }
        return "1".equals(statusModify.getFrom()) && "2".equals(statusModify.getTo());
    }

    @Override
    public void handle(Issue issue) {
        // TODO 当状态从1到2的时候，采取的处理方式
    }

}
