package com.example.demo.transaction;

import java.util.Objects;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-09 16:25
 **/
public class IssueDeleteStrategy implements IssueStrategy {

    @Override
    public boolean isMatched(StatusModify statusModify){
        if(Objects.isNull(statusModify)) {
            return false;
        }
        return "5".equals(statusModify.getFrom()) && "6".equals(statusModify.getTo());
    }

    @Override
    public void handle(Issue issue){
        // TODO 当状态从5到6的时候，采取的处理方式
    }
}
