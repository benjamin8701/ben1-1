package com.example.demo.transaction;

import lombok.Data;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-09 15:58
 **/
@Data
public class StatusModify {

    private String from;

    private String to;

}
