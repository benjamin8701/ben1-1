package com.example.demo.models;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author raylax
 */
@Data
public class BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @TableField(fill = FieldFill.INSERT)
    private String creator;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    @TableField(fill = FieldFill.UPDATE)
    private String updater;
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateAt;

    @TableLogic
    private boolean deleted;

    @Version
    private Integer version;

}
