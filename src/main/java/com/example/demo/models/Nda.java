package com.example.demo.models;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author raylax
 */
@Data
@TableName("t_nda")
public class Nda extends BaseEntity {

    /**
     * 签约主体
     */
    private String contractSubject;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目Code
     */
    private String projectCode;

    /**
     * 币种
     */
    private String currency;

    /**
     * Possible deal size
     */
    private String possibleDealSize;

    /**
     * 预估范围
     */
    private String rangeOfEstimates;

    /**
     * 简介
     */
    private String summary;

    /**
     * Investment team name
     */
    private String investmentTeamName;

    /**
     * Investment team id
     */
    private String investmentTeamId;

    /**
     * 对接人
     */
    private String brokerName;

    /**
     * 对接人邮箱
     */
    private String brokerEmail;

    /**
     * 对接人微信号
     */
    private String brokerWechat;

    /**
     * 对接人手机号
     */
    private String brokerPhone;

    /**
     * 是否有上市公司关联方
     */
    private Integer hasRelatedParties;

    /**
     * Notes
     */
    private String notes;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 模版类型
     */
    private String templateTypeId;

    /**
     * 模版名称
     */
    private String templateTypeName;

    /**
     * 模版提交人
     */
    private String templateTypeSubmitBy;

    /**
     * 申请人
     */
    private String applicant;

    /**
     * Deal Team审批人ID
     */
    private String dealApproverId;

    /**
     * Deal Team审批人名
     */
    private String dealApproverName;

    /**
     * Legal处理人ID
     */
    private String legalProcessorId;

    /**
     * Legal处理人名
     */
    private String legalProcessorName;

    /**
     * Legal Team审批人ID
     */
    private String legalApproverId;

    /**
     * Legal Team审批人名
     */
    private String legalApproverName;

    /**
     * 是否使用HH模版
     */
    private Boolean useHhTemplate;

    /**
     * NDA期限
     */
    private String ndaDeadline;

    /**
     * 附加条款
     */
    private String additionalTerm;

    /**
     * 是否加入NL
     */
    private Integer joinRl;

    /**
     * 存档时间
     */
    private LocalDateTime archiveTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * Deal Id
     */
    private String dealId;

    /**
     * Deal name
     */
    private String dealName;

    /**
     * deal team 提交时间
     */
    private LocalDateTime dealSubmitAt;

    /**
     * legal team 提交时间
     */
    private LocalDateTime legalSubmitAt;

    /**
     * 是否紧急
     */
    private Boolean urgent;

    /**
     * 是否紧急
     */
    private Boolean legalUrgent;

    private Integer urgentValue;

    /**
     * 是否需要原件
     */
    private Boolean needOriginal;

    private String groupUserId;

    private String groupUserName;

    private String postalAddress;

    private String logisticsCompany;

    private String logisticsNo;

    private String ticker;

    private Boolean addedToRl;

    private String copies;

    private Integer vc;

    private Boolean received;

    private Boolean offline;

    private Boolean quickApproval;

    private Integer templateKind;

    private String parentId;

    private Boolean masterExisted;

    private Integer childrenCount;

    private Integer childrenArchivedCount;

}
