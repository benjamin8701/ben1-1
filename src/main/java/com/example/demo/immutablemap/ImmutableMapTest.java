package com.example.demo.immutablemap;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-12 10:02
 **/
public class ImmutableMapTest {

    public static void main(String[] args) {
        Map<String, String> of = ImmutableMap.of("key1", "value1", "key2", "value2");
    }
}
