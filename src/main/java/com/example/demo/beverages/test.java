package com.example.demo.beverages;

/**
 * @program: ben1-11
 * @description:
 * @author: wb
 * @create: 2021-08-09 10:14
 **/
public class test {

    public static void main(String[] args) {
        Beverages ice = new suger(new Ice(new Cola("可乐")));
        System.out.println(ice.getDescription());
        System.out.println(ice.price());

        Beverages suger = new suger(new suger(new Ice(new Cola("可乐"))));
        System.out.println(suger.getDescription());
        System.out.println(suger.price());
    }
}
