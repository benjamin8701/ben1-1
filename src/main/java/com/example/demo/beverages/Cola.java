package com.example.demo.beverages;

import org.springframework.transaction.event.TransactionalEventListener;

/**
 * @program: ben1-11
 * @description: 可乐
 * @author: wb
 * @create: 2021-08-09 10:07
 **/
public class Cola extends Beverages {

    public Cola(String description) {
        this.description = description;
    }

    @Override
    @TransactionalEventListener
    public double price() {
        return 2.0;
    }
}
