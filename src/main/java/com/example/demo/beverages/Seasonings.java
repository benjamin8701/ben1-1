package com.example.demo.beverages;

/**
 * @program: ben1-11
 * @description: 配料
 * @author: wb
 * @create: 2021-08-09 10:02
 **/
public abstract class Seasonings extends Beverages {
    public Beverages beverages;
    public abstract String  getDescription();
}
