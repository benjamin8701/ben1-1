package com.example.demo.beverages;

/**
 * @program: ben1-11
 * @description: 加糖
 * @author: wb
 * @create: 2021-08-09 10:19
 **/
public class suger extends Seasonings {

    public suger(Beverages beverages) {
        this.beverages = beverages;
    }

    @Override
    public String getDescription() {
        return beverages.getDescription() + "加糖";
    }

    @Override
    public double price() {
        return 0.5 + beverages.price();
    }
}
