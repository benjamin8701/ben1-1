package com.example.demo.beverages;

/**
 * @program: ben1-11
 * @description: 配料加冰
 * @author: wb
 * @create: 2021-08-09 10:02
 **/
public class Ice extends Seasonings {

    public Ice(Beverages beverages) {
        this.beverages = beverages;
    }

    @Override
    public String getDescription() {
        return beverages.getDescription() + "加冰";
    }

    @Override
    public double price() {
        return beverages.price() + 0.2;
    }
}
