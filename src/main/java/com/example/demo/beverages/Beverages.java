package com.example.demo.beverages;

/**
 * @program: ben1-11
 * @description: 饮料
 * @author: wb
 * @create: 2021-08-09 10:00
 **/
public abstract class Beverages {

    public String description;

    public String getDescription() {
        return description;
    }

    public abstract double price();
}
