package com.example.demo.longtest;


import java.time.LocalDateTime;

/**
 * @program: ben1-1
 * @description: long
 * @author: wb
 * @create: 2021-08-04 16:43
 **/
public class LongTest {

    public static void main(String[] args) {
        System.out.println(LocalDateTime.now());
        sum();
        System.out.println(LocalDateTime.now());
    }

    private static long sum() {
        Long l = 0L;
        for (long i = 0; i < Integer.MAX_VALUE; i++) {
            l = l + i;
        }
        return l;
    }
}
