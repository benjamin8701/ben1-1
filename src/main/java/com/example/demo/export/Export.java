package com.example.demo.export;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.alibaba.excel.write.metadata.fill.FillWrapper;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Export.
 *
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021 -08-11 14:08
 */
@RestController
@RequestMapping("/personal/policy")
@Slf4j
public class Export {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");


    @ApiImplicitParams({
            @ApiImplicitParam(value = "用户id", name = "userId")
    })
    @ApiOperation("导出")
    @GetMapping("/export")
    public void export(
            HttpServletResponse response,
            @RequestParam(value = "userId", required = false) String userId) throws Exception {
        final List<PersonalPolicyResponse> list = new ArrayList<>();

        PersonalPolicyResponse personalPolicyResponse1 = new PersonalPolicyResponse();
        personalPolicyResponse1.setUserNameEn("1");
        personalPolicyResponse1.setUserNameCn("1");
        personalPolicyResponse1.setUserEmail("1");
        personalPolicyResponse1.setFileId("1");
        personalPolicyResponse1.setFileName("1");
        personalPolicyResponse1.setFileShortName("1");
        personalPolicyResponse1.setFullUserName("1");

        final List<PersonalPolicyResponse> list2 = new ArrayList<>();
        PersonalPolicyResponse personalPolicyResponse2 = new PersonalPolicyResponse();
        personalPolicyResponse2.setUserNameEn("2");
        personalPolicyResponse2.setUserNameCn("2");
        personalPolicyResponse2.setUserEmail("2");
        personalPolicyResponse2.setFileId("2");
        personalPolicyResponse2.setFileName("2");
        personalPolicyResponse2.setFileShortName("2");
        personalPolicyResponse2.setFullUserName("2");
        list.add(personalPolicyResponse1);
        list2.add(personalPolicyResponse2);

        Resource resource = new ClassPathResource("file/PersonalPoliciesTemplate.xlsx");
        try {
            ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).withTemplate(resource.getInputStream()).build();
            WriteSheet writeSheet = EasyExcel.writerSheet().build();
            FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
            excelWriter.fill(new FillWrapper("data1",list), fillConfig, writeSheet);
            excelWriter.fill(new FillWrapper("data2",list2), fillConfig, writeSheet);
            excelWriter.finish();
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            String fileName = URLEncoder.encode("Personal Policies " + DATE_TIME_FORMATTER.format(LocalDateTime.now()), "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        } catch (Exception e) {
            log.info("e:{}", e.toString());
        }
    }
}
