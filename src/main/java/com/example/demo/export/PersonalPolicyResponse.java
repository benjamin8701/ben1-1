package com.example.demo.export;

import lombok.Data;


/**
 * @author raylax
 */
@Data
public class PersonalPolicyResponse {

    private String userNameEn;

    private String userNameCn;

    private String userEmail;

    private String fileId;

    private String fileName;

    private String fileShortName;

    private String fullUserName;

}
