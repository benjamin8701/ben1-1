package com.example.demo.bean;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.dozer.loader.api.TypeMappingOptions.mapNull;

/**
 * DozerBeanMapperImpl.
 *
 * @author raylax
 */
@Component
public class DozerBeanMapperImpl implements BeanMapper {

    private static final DozerBeanMapper MAPPER = new DozerBeanMapper();
    private static final DozerBeanMapper SKIP_NULL_MAPPER = new DozerBeanMapper();
    static {
        List<String> mappingFiles = Collections.singletonList("dozerJdk8Converters.xml");
        MAPPER.setMappingFiles(mappingFiles);
        SKIP_NULL_MAPPER.setMappingFiles(mappingFiles);
        SKIP_NULL_MAPPER.addMapping(new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping("hillinsight.nda.api.dto.ChildNdaUpdateRequest", "hillinsight.nda.api.models.Nda", mapNull(false));
            }
        });
    }


    @Override
    public <T> T map(Object source, Class<T> klass) {
        if (source == null) {
            return null;
        }
        return MAPPER.map(source, klass);
    }

    @Override
    public void map(Object source, Object target) {
        if (source == null || target == null) {
            return;
        }
        MAPPER.map(source, target);
    }

    @Override
    public void mapSkipNull(Object source, Object target) {
        if (source == null || target == null) {
            return;
        }
        SKIP_NULL_MAPPER.map(source, target);
    }

    @Override
    public <T> List<T> mapList(List<?> source, Class<T> klass) {
        if (source == null) {
            return null;
        }
        if (source.isEmpty()) {
            return new ArrayList<>();
        }
        List<T> target = new ArrayList<>(source.size());
        for (Object s : source) {
            target.add(map(s, klass));
        }
        return target;
    }

    @Override
    public void mapList(List<?> source, List<?> target) {
        if (source == null || target == null || source.size() != target.size()) {
            return;
        }
        for (int i = 0; i < source.size(); i++) {
            map(source.get(i), target.get(i));
        }
    }

    @Override
    public <T> Page<T> mapPage(IPage<?> source, Class<T> klass) {
        Page<T> r = new Page<>();
        r.setRecords(null);
        map(source, r);
        r.setRecords(mapList(source.getRecords(), klass));
        return r;
    }

    @Override
    public <T> void mapPage(IPage<?> source, IPage<T> target) {
        map(source, target);
        mapList(source.getRecords(), target.getRecords());
    }

}
