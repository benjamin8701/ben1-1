package com.example.demo.bean;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * BeanMapper.
 *
 * @author raylax
 */
public interface BeanMapper {

    /**
     * map 转换.
     *
     * @param source s
     * @param klass  k
     * @param <T>    t
     * @return r
     */
    <T> T map(Object source, Class<T> klass);

    /**
     * map 转换.
     *
     * @param source s
     * @param target t
     */
    void map(Object source, Object target);

    /**
     * map 转换.
     *
     * @param source s
     * @param target t
     */
    void mapSkipNull(Object source, Object target);

    /**
     * maplist转换.
     *
     * @param source s
     * @param klass  k
     * @param <T>    t
     * @return r
     */
    <T> List<T> mapList(List<?> source, Class<T> klass);

    /**
     * maplist转换.
     *
     * @param source s
     * @param target t
     */
    void mapList(List<?> source, List<?> target);

    /**
     * mapPage转换.
     *
     * @param source s
     * @param klass  k
     * @param <T>    t
     * @return t
     */
    <T> Page<T> mapPage(IPage<?> source, Class<T> klass);

    /**
     * mapPage转换.
     *
     * @param source s
     * @param target t
     * @param <T>    t
     */
    <T> void mapPage(IPage<?> source, IPage<T> target);

}
