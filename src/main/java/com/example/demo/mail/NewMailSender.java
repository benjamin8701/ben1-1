package com.example.demo.mail;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-12 14:29
 **/
@Slf4j
@Component
public class NewMailSender {

    @Autowired
    private MailConfig mailConfig;

    @Autowired
    private JavaMailSender mailSender;

    public void send(List<String> to, Object data, MailTemplate.Config templateConfig, List<MailAttachment> attachments) {
        log.debug("发送Email消息 to:[{}]", Strings.join(to, ','));
        List<String> toUsers = to.stream().map(String::trim).filter(StringUtils::isNotEmpty).collect(Collectors.toList());
        if (toUsers.isEmpty()) {
            return;
        }
        final MimeMessage message = mailSender.createMimeMessage();
        try {
            final MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
            messageHelper.setSubject(Optional.ofNullable(templateConfig.getSubject()).orElse(mailConfig.getSubject()));
            messageHelper.setFrom(mailConfig.getSender(), mailConfig.getFrom());
            if (toUsers.size() == 1) {
                messageHelper.setTo(new String[]{toUsers.get(0)});
            } else {
                messageHelper.setTo(toUsers.toArray(new String[0]));
            }
            if (CollectionUtils.isNotEmpty(attachments)) {
                for (MailAttachment attachment : attachments) {
                    messageHelper.addAttachment(attachment.getName(), attachment.getSource());
                }
            }
            messageHelper.setText(TemplateUtils.process(templateConfig.getFile(), data), true);
            mailSender.send(messageHelper.getMimeMessage());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static void main(String[] args) {
        NewMailSender newMailSender = new NewMailSender();
        newMailSender.send(Lists.newArrayList("wb19900725@gmail.com"),
                "aaaaaa",MailTemplate.T_05(),null);
    }
}
