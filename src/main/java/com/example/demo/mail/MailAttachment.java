package com.example.demo.mail;

import lombok.Data;
import org.springframework.core.io.InputStreamSource;

@Data
public class MailAttachment {

    private String name;

    private InputStreamSource source;

}
