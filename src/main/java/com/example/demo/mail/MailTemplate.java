package com.example.demo.mail;

import lombok.Data;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-12 14:16
 **/
public class MailTemplate {

    public static final String TO_LEGAL_LEADER_APPROVED = "hlink_template_to_legal_leader_approved.ftl";

    public static void main(String[] args) {

    }

    public static Config T_05() {
        return Config.make("hlink_template_to_legal_leader_approved.ftl", "Test");
    }

    @Data
    public static class Config {
        private String file;
        private String subject;
        private String suffix;

        private Config(String file, String subject) {
            this.file = file;
            this.subject = subject;
        }

        static Config make(String file, String subject) {
            return new Config(file, subject);
        }

        public Config suffix(String suffix) {
            this.suffix = suffix;
            return this;
        }

        public String getSubject() {
            return suffix == null ? subject : (subject + " - " + suffix);
        }
    }
}
