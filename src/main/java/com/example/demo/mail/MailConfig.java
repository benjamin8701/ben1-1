package com.example.demo.mail;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @program: demo
 * @description:
 * @author: wb
 * @create: 2021-08-12 14:27
 **/
@Data
@Component
@ConfigurationProperties(prefix = "mail")
public class MailConfig {

    private String subject;

    private String sender;

    private String from;
}
