package com.example.demo.mail;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.StringWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author raylax
 */
public class TemplateUtils {

    private static final Configuration CONFIGURATION = new Configuration(Configuration.VERSION_2_3_21);
    private static final Map<String, Template> TEMPLATE_CACHE = new ConcurrentHashMap<>();

    static {
        CONFIGURATION.setTemplateLoader(new ClassTemplateLoader(NewMailSender.class,"/template/"));
    }

    public static String process(String templateFile, Object data) {
        final Template template = TEMPLATE_CACHE.computeIfAbsent(templateFile, TemplateUtils::getTemplate);
        try (final StringWriter writer = new StringWriter()) {
            template.process(data, writer);
            return writer.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private static Template getTemplate(String templateFile) {
        try {
            return CONFIGURATION.getTemplate(templateFile);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
