package com.example.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author raylax
 */
@Data
public class NdaCreateUpdateRequest {

    private String id;

    @ApiModelProperty("签约主体")
    @NotEmpty
    private String contractSubject;

    @ApiModelProperty("项目名称")
    @NotEmpty
    private String projectName;

    @ApiModelProperty("项目Code")
    @NotEmpty
    private String projectCode;

    @ApiModelProperty("币种")
    @NotEmpty
    private String currency;

    @ApiModelProperty("Possible deal size")
    private String possibleDealSize;

    @ApiModelProperty("预估范围")
    private String rangeOfEstimates;

    @ApiModelProperty("简介")
    private String summary;

    @ApiModelProperty("Investment team name")
    @NotEmpty
    private String investmentTeamName;

    @ApiModelProperty("Investment team id")
    @NotEmpty
    private String investmentTeamId;

    @ApiModelProperty("对接人")
    private String brokerName;

    @ApiModelProperty("对接人邮箱")
    private String brokerEmail;

    @ApiModelProperty("对接人微信号")
    private String brokerWechat;

    @ApiModelProperty("对接人手机号")
    private String brokerPhone;

    @ApiModelProperty("是否有上市公司关联方")
    private Integer hasRelatedParties;

    @ApiModelProperty("Notes")
    private String notes;

    @ApiModelProperty("状态")
    private Integer status;

    @ApiModelProperty("模版类型")
    @NotEmpty
    private String templateTypeId;

    @ApiModelProperty("模版名称")
    @NotEmpty
    private String templateTypeName;

    @ApiModelProperty("模版提交人")
    @NotEmpty
    private String templateTypeSubmitBy;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("Deal Id")
    private String dealId;

    @ApiModelProperty("Deal Name")
    private String dealName;

    @ApiModelProperty("是否紧急")
    private Boolean urgent;

    @ApiModelProperty("是否需要原件")
    private Boolean needOriginal;

    @ApiModelProperty("Investment Group User Id")
    private String groupUserId;

    @ApiModelProperty("Investment Group User Name")
    private String groupUserName;

    @ApiModelProperty("Ticker")
    private String ticker;

    @ApiModelProperty("是否是VC NDA")
    private Integer vc;

    private int version;

    @ApiModelProperty("模板类型（大类）")
    private Integer templateKind;

    @ApiModelProperty("主NDA是否存在")
    private Boolean masterExisted;

    @ApiModelProperty("子NDA列表")
    private List<ChildNdaUpdateRequest> children;

}
