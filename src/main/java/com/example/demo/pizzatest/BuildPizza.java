package com.example.demo.pizzatest;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: ben1-1
 * @description: build
 * @author: wb
 * @create: 2021-08-02 16:33
 **/
public class BuildPizza {

    public void buildPizza() {
        CalZone calZone = new CalZone.Builder().addTopping(Pizza.Topping.HAM).sauceInside().build();
    }

    public static void main(String[] args) {
        List<Topping> topping = null;
        Map<String, List<Topping>> collect =
                topping.stream().collect(Collectors.groupingBy(Topping::getHam));
        System.out.println(collect);
    }
}
