package com.example.demo.pizzatest;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

/**
 * @program: ben1-1
 * @description: pizza
 * @author: wb
 * @create: 2021-08-02 14:35
 **/
public abstract class Pizza {
    public enum Topping {HAM, MUSHROOM, ONION, PEPPER, SAUSAGE}

    final Set<Topping> toppings;

    abstract static class Builder<T extends Builder<T>> {
        EnumSet<Topping> toppings = EnumSet.noneOf(Topping.class);

        public T addTopping(Topping topping) {
            toppings.add(Objects.requireNonNull(topping));
            return self();
        }

        abstract Pizza build();

        protected abstract T self();
    }

    Pizza(Builder<?> builder) {
        toppings = builder.toppings.clone();
    }
}

