package com.example.demo.pizzatest;

import sun.swing.plaf.synth.DefaultSynthStyle;

import java.util.Objects;

/**
 * @program: ben1-1
 * @description: NyPizza
 * @author: wb
 * @create: 2021-08-02 15:16
 **/
public class NyPizza extends Pizza {
    public enum Size {SMALL, MEDIUM, LARGE}

    private final Size size;

    public static class Builder extends Pizza.Builder<Builder> {
        private final Size size;

        public Builder(Size size) {
            this.size = Objects.requireNonNull(size);
        }

        @Override
        public NyPizza build() {
            return new NyPizza(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }

    private NyPizza(Builder builder) {
        super(builder);
        size = builder.size;
    }
}
