package com.example.demo.pizzatest;

import lombok.Builder;
import lombok.Data;

/**
 * @program: ben1-1
 * @description: topping
 * @author: wb
 * @create: 2021-08-02 16:08
 **/
@Data
@Builder
public class Topping {
    public final String ham;
    public final String mushroom;
    public final String onion;
    public final String pepper;
    public final String sausage;
}
