package com.example.demo.pizzatest;

import lombok.Data;

/**
 * @program: ben1-1
 * @description: size
 * @author: wb
 * @create: 2021-08-02 16:31
 **/
@Data
public class Size {
    private String small = "SMALL";
    private String medium = "MEDIUM";
    private String large = "LARGE";
}
