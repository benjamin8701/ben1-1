Legal team(${nda.legalProcessorName}) received a new
<#if nda.legalUrgent??>${nda.legalUrgent?string("<font color='red'>urgent</font> ", "")}</#if>NDA
request<#if nda.templateKind?? && nda.templateKind==30>${nda.dealName???string('('+nda.dealName+')','')}<#else>${nda.projectName???string('('+nda.projectName+')','')}</#if>,
from ${nda.investmentTeamName} team(${nda.applicant}),
please click to view.